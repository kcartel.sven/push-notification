const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
mongoose.Promise  = require("bluebird");
const functions = require('firebase-functions');
const port = 3000;
const url = 'mongodb+srv://user_mongodb:user_mongodb@clustercrud-vhqxe.mongodb.net/crud?retryWrites=true&w=majority';

//  routes
const trainerRouter = require('./routes/trainer.route');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/trainers', trainerRouter);

// mongoose.connect(url, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// },
// function(error){
//     if(error) console.log(error);
//     // console.log("connection successful");
// });

exports.app = functions.https.onRequest(app)

// server.listen(port, () => {
//     console.log('Server running on port ' + port);
// });

// app.listen(process.env.PORT || 3001, () => { console.log(`Server is running n port ${process.env.PORT}`) });