const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const PORT = 3000;
const app = express();
const functions = require('firebase-functions');
const admin = require('./firebase-config');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.get('/hello', (req,res,next) => {
    res.send('Welcome to Firebase functions with Node Express');
});

app.post('/sendNotification', async (req, res) => {

    try {
        const notification_options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };

        // web
        // const registrationToken = 'dvvyLJ6p2pBM1fCNj9EVzv:APA91bGspOp1sEYwymWPxMDZ_CTeIO7l571HOw4QLoZXN97FN6hH8u0SHAdy09QepKRJWkGz9CzhK98IqhrA3LqWKLPPmPCFPBMgxmYERd3OpbNS-e7ykw2aKGNxw22bBJZtgpAO2dKB';
        // mobile
        // const registrationToken = 'fPSZyMYWT96vrZUjWUBgSI:APA91bGw78qwHH3TjuQa1pkDm_vw19EKU6KZH5N__DcL_rAPMAkdRlm1wn1kZdTlVH-OxZYbn1BVaBPSIeFym0rjMWWyMV7C1wuSl4eFGTQunTiE3mCoftxhIejeeTwz1-3FFvDGAtIf';
        // new
        // const registrationToken = 'c212zSCZF-3z-W0dWzTFkf:APA91bEhVaBBi_F49Wep6-6q-8f8PEapoeXlpf1s3b_SnEYKMcANcXoFY1qmlqSOPqhkwFszJjOhDNG1ExGno3ILKiklMoibkHsGXq_v-g4Rkdt8lAPkPkpoIaAsJFa0kvS7Gc7ySbSU';
        const registrationToken = await req.body.regToken;
        const messageTitle = await req.body.name;
        const messageDescription = await req.body.description;

        const message_notification = {
            notification: {
                title: messageTitle,
                body: messageDescription
            }
        };

        await admin.firestore().collection('tmp-notifsv1-devv1-fs-db')
        .add({
            name: messageTitle,
            description: messageDescription,
            created: new Date(Date.now())
        });

        await admin.messaging().sendToDevice(registrationToken, message_notification, notification_options);

        res.status(200).send();
        
    } catch (error) {
        console.log(error);
        res.status(500).send(error)
    }
});

app.listen(PORT, () => {
    console.log('Server is running on PORT', PORT)
});

exports.app = functions.https.onRequest(app);


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });


// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
