const trainerModel = require('../models/trainer.model');
const admin = require('../firebase-config');

exports.trainer_get = async (req, res) => {
    const trainer = await trainerModel.find({});
    try {
        res.send(trainer);
    } catch (err) {
        res.status(500).send(err);
    }
}

exports.trainer_create = async (req, res) => {
    const trainer = new trainerModel(req.body);

    try {
        await trainer.save();
        res.status(200).send(trainer);
    } catch (err) {
        res.status(500).send(err);
    }
}

exports.trainer_delete = async (req, res) => {
    try {
        const trainer = await trainerModel.findByIdAndDelete(req.params.id);

        if (!trainer) res.status(404).send("No item found")
        res.status(200).send()

    } catch (err) {
        res.status(500).send(err);
    }
}

exports.trainer_test = async (req, res) => {

    try {
        const notification_options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };

        const registrationToken = 'fPSZyMYWT96vrZUjWUBgSI:APA91bGw78qwHH3TjuQa1pkDm_vw19EKU6KZH5N__DcL_rAPMAkdRlm1wn1kZdTlVH-OxZYbn1BVaBPSIeFym0rjMWWyMV7C1wuSl4eFGTQunTiE3mCoftxhIejeeTwz1-3FFvDGAtIf';

        const message_notification = {
            notification: {
                title: req.body.name,
                body: req.body.description
            }
        };

        await admin.firestore().collection('Notifications')
        .add({
            name: req.body.name,
            description: req.body.description,
            created: new Date(Date.now())
        });

        await admin.messaging().sendToDevice(registrationToken, message_notification, notification_options);

        res.status(200).send();
        
    } catch (error) {
        console.log(error);
        res.status(500).send(error)
    }
}

exports.trainer_update = async (req, res) => {
    try {
        const trainer = await trainerModel.findByIdAndUpdate(req.params.id, req.body);
        await trainerModel.save();
        res.send(trainer)
    } catch (err) {
        res.status(500).send(err);
    }
}