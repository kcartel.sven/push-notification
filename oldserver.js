const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
mongoose.Promise  = require("bluebird");
const http = require("http");
const socketIo = require("socket.io");
const server = http.createServer(app);
const io = socketIo(server);
const axios = require("axios");

const port = 3000;
const url = 'mongodb+srv://user_mongodb:user_mongodb@clustercrud-vhqxe.mongodb.net/crud?retryWrites=true&w=majority';

//  routes
const trainerRouter = require('./routes/trainer.route');

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

const trainerModel = require('./models/trainer.model');

let interval;

io.on("connection", (socket) => {
  console.log("New client connected");
  if (interval) {
    clearInterval(interval);
  }

  let pageNo = socket.handshake.query['pageNo'];
  let size = socket.handshake.query['size'];
  let singleId = socket.handshake.query['singleId']

  interval = setInterval(() => getApiAndEmit(socket, pageNo, size, singleId), 1000);
  
  socket.on("disconnect", () => {
    console.log("Client disconnected");
    clearInterval(interval);
  });
});

// io.on('connection', (socket) => {
//   console.log("New client connected");

//     getApiAndEmit(socket)
//     socket.on("disconnect", () => {
//             console.log("Client disconnected");
//     });
// });

const getApiAndEmit = async (socket, pageNo, size, id) => {
    // trainerModel.find({}, function(err, trainer) {
    //     if(err) throw err;
    //     socket.emit("FromAPI", trainer);
    // });
    try {
      const res = await axios.get(
        `http://localhost:3001/students?pageNo=${pageNo}&size=${size}&id=${id}`
      );
      socket.emit("FromAPI", res.data);
    } catch (error) {
      console.error(`Error: ${error.code}`);
    }
}

// io.on('connection', (socket) =>{
//     console.log('a user is connected');

//     trainerModel.find({}, function(err, trainer) {
//         if(err) throw err;
//         // console.log(trainer);
//         // socket.on('trainer data', trainer => {
//         //     // console.log('data =>' , trainer);
//         //     socket.emit('trainer data', trainer);
//         // })
//         socket.emit("trainer data", trainer);
//      });

//     // socket.on('trainer data', msg => {
//     // })
// });

app.use('/trainers', trainerRouter);


mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
},
function(error){
    if(error) console.log(error);
    // console.log("connection successful");
});

server.listen(port, () => {
    console.log('Server running on port ' + port);
});
// app.listen(process.env.PORT || 3001, () => { console.log(`Server is running n port ${process.env.PORT}`) });