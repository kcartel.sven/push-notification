
const admin = require('firebase-admin');

var serviceAccount = require("./permissions.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://toyota-ph.firebaseio.com"
});

// firebase.initializeApp({
//   apiKey: 'AAAALtf-FYI:APA91bEyuj9nUPUPPtEpo2Rz3LkCpHLKhPbShxLC8IBGBxZQrSHNkPA_TvU2vRvMRbKfu2nGCEwVCMp8xFK-WH3M-K4YLqUPuu8Tp37Oe72e1mQHLKdOEFW3DdXGmZGoUdfNAo_iPbPL',
//   authDomain: 'https://pushnotification-b4811.firebaseio.com',
//   projectId: 'pushnotification-b4811',
//   storageBucket: 'pushnotification-b4811.appspot.com',
//   messagingSenderId: '201192248706',
//   // credential: admin.credential.cert(serviceAccount),
//   databaseURL: "https://pushnotification-b4811.firebaseio.com"
// });

module.exports = admin;

